======================================================== 
==	Configurando um DataSource com Postgres no Widfly ==
========================================================
1. Criar a pasta postgres/main dentro de \modules\system\layers\base\org
2. Copiar o jar do postgres para pasta recem criada
3. Criar o arquivo modelu.xml conforme abaixo:
	
	<module xmlns="urn:jboss:module:1.0" name="org.postgres">
		  <resources>
			<resource-root path="postgresql-9.3-1102-jdbc41.jar"/>
		  </resources>
		   <dependencies>
			 <module name="javax.api"/>
			 <module name="javax.transaction.api"/>
		   </dependencies>
	</module>
	
4. Editar o arquivo standalone.xml do Windfly, localizado em standalone\configuration, e incluir a linhas abaixo:
	
	<datasource jta="true" jndi-name="java:jboss/datasources/K12DS" pool-name="java:jboss/datasources/K12DS_Pool" enabled="true" use-java-context="true" use-ccm="true">
		<connection-url>jdbc:postgresql://localhost:5432/db_k12</connection-url>
		<driver>postgresql</driver>
		<security>
			<user-name>postgres</user-name>
			<password>postgres</password>
		</security>
	</datasource>
	
	<driver name="postgresql" module="org.postgres">
		<xa-datasource-class>org.postgresql.xa.PGXADataSource</xa-datasource-class>
	</driver>

5. Registar o module recem criado no hibernate em modules\system\layers\base\org\hibernate\main\module.xml
	<module name="org.postgres"/>
