package br.com.wood.k12.model.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.wood.k12.model.Carro;

public class CarroRepository {
	private EntityManager manager;
	
	public CarroRepository(EntityManager manager) {
		this.manager = manager;
	}
	
	public void adciona(Carro carro){
		this.manager.persist(carro);
	}
	
	@SuppressWarnings("unchecked")
	public List<Carro> buscaTodos() {
		Query query = this.manager.createQuery("SELECT c FROM Carro c");
		return query.getResultList();
	}
}
