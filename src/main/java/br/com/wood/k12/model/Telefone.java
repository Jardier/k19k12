package br.com.wood.k12.model;

public class Telefone {
	private String codigoPais;
	private String codigoDeArea;
	private String numeroLocal;

	public Telefone() {

	}

	public Telefone(String codigoPais, String codigoDeArea, String numeroLocal) {
		super();
		this.codigoPais = codigoPais;
		this.codigoDeArea = codigoDeArea;
		this.numeroLocal = numeroLocal;
	}

	public String getCodigoPais() {
		return codigoPais;
	}

	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}

	public String getCodigoDeArea() {
		return codigoDeArea;
	}

	public void setCodigoDeArea(String codigoDeArea) {
		this.codigoDeArea = codigoDeArea;
	}

	public String getNumeroLocal() {
		return numeroLocal;
	}

	public void setNumeroLocal(String numeroLocal) {
		this.numeroLocal = numeroLocal;
	}

}
