package br.com.wood.k12.model;

public class Cpf {
	private Integer numeroDeIdentificacao;
	private Integer primeiroDigitorVerificador;
	private Integer segundoDigitorVerificador;
	public Integer getNumeroDeIdentificacao() {
		return numeroDeIdentificacao;
	}
	public void setNumeroDeIdentificacao(Integer numeroDeIdentificacao) {
		this.numeroDeIdentificacao = numeroDeIdentificacao;
	}
	public Integer getPrimeiroDigitorVerificador() {
		return primeiroDigitorVerificador;
	}
	public void setPrimeiroDigitorVerificador(Integer primeiroDigitorVerificador) {
		this.primeiroDigitorVerificador = primeiroDigitorVerificador;
	}
	public Integer getSegundoDigitorVerificador() {
		return segundoDigitorVerificador;
	}
	public void setSegundoDigitorVerificador(Integer segundoDigitorVerificador) {
		this.segundoDigitorVerificador = segundoDigitorVerificador;
	}

	

}
