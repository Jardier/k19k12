package br.com.wood.k12.model;

import java.util.Date;

import javax.validation.constraints.NotNull;

public class Funcionario {
	private String nome;

	@NotNull(message="{br.com.wood.k12.nulo}")
	private String sobreNome;

	private Double salario;
	private Integer codigo;
	private Date aniversario;

	public String getSobreNome() {
		return sobreNome;
	}

	public void setSobreNome(String sobreNome) {
		this.sobreNome = sobreNome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getSalario() {
		return salario;
	}

	public void setSalario(Double salario) {
		this.salario = salario;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Date getAniversario() {
		return aniversario;
	}

	public void setAniversario(Date aniversario) {
		this.aniversario = aniversario;
	}

}
