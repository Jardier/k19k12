package br.com.wood.k12.helper;

import javax.annotation.Resource;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

public class JPAProducer {

	@PersistenceContext
	@Produces
	private EntityManager em;
	

}
