package br.com.wood.k12.filters;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

//@WebFilter(servletNames="Faces Servlet")
public class JPAFilter implements Filter {
	 private final static String UNIT_NAME = "K19-PU";
	
	@PersistenceUnit(name=UNIT_NAME)
	private EntityManagerFactory factory;

	
	public void init(FilterConfig filterConfig) throws ServletException {
		this.factory = Persistence.createEntityManagerFactory("K19-PU");

	}


	public void destroy() {
		this.factory.close();

	}


	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// CHEGADA
		EntityManager manager = this.factory.createEntityManager();
		request.setAttribute("EntityManager", manager);
		manager.getTransaction().begin();
		
		//FACES SERVLET
		chain.doFilter(request, response);
		
		try{
			manager.getTransaction().commit();
		}catch(Exception ex){
			manager.getTransaction().rollback();
		}finally{
			manager.close();
		}

	}

}
