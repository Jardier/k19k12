package br.com.wood.k12.persistence.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Dog implements Serializable {
	private static final long serialVersionUID = 7693292072445549368L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String nome;
	private String raca;
	private String proprietario;

	public Dog() {
		super();
	}

	public Dog(String nome, String raca, String proprietario) {
		super();
		this.nome = nome;
		this.raca = raca;
		this.proprietario = proprietario;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRaca() {
		return raca;
	}

	public void setRaca(String raca) {
		this.raca = raca;
	}

	public String getProprietario() {
		return proprietario;
	}

	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}

	@Override
	public String toString() {
		return "Dog [id=" + id + ", nome=" + nome + ", raca=" + raca
				+ ", proprietario=" + proprietario + "]";
	}

}
