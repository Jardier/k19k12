package br.com.wood.k12.persistence.entity;

public enum Periodo {
	MATUTINO,
	VESPERTINO,
	NOTURNO
}
