package br.com.wood.k12.persistence.dao.impl;

import java.io.Serializable;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import br.com.wood.k12.persistence.dao.DAO;

@ManagedBean
@RequestScoped
public abstract class DAOImp<T, I extends Serializable> implements DAO<T,I> {
	
	@Inject
	private EntityManager em;

	@Resource
	private UserTransaction tx;
	
	@Override
	@PostConstruct
	public T save(T entity) {
		T saved = null;
		
		try {
			tx.begin();
			saved = em.merge(entity);
			tx.commit();
		} catch (Exception e) {
			try{
				tx.setRollbackOnly();
			} catch(SystemException e1){
				e1.printStackTrace();
			}
		} 
		
		return saved;
	}

}
