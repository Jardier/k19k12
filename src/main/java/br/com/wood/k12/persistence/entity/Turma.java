package br.com.wood.k12.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Persistence;
import javax.persistence.Table;

@Entity
@Table
public class Turma {
	@Id
	@GeneratedValue
	private Long id;

	@Enumerated(EnumType.STRING)
	private Periodo periodo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Periodo getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}
	
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("dbk12");
		EntityManager manager = factory.createEntityManager();
		
		manager.getTransaction().begin();
		
		Turma t1 = new Turma();
		t1.setPeriodo(Periodo.NOTURNO);
		
		Turma t2 = new Turma();
		t2.setPeriodo(Periodo.VESPERTINO);
		
		manager.persist(t1);
		manager.persist(t2);
		
		manager.getTransaction().commit();
	}

}
