package br.com.wood.k12.persistence.dao;

import br.com.wood.k12.persistence.entity.Editora;

public interface EditoraDAO extends DAO<Editora, Integer>{

}
