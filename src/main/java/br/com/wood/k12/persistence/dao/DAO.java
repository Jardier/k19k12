package br.com.wood.k12.persistence.dao;

import java.io.Serializable;

public interface DAO<T, I extends Serializable> {
	
	public T save(T entity);
}
