package br.com.wood.k12.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import br.com.wood.k12.model.Carro;
import br.com.wood.k12.model.repository.CarroRepository;

@ManagedBean
public class CarroMB {
	private Carro carro = new Carro();
	
	public void adicionaCarro(){
		EntityManager manager = this.getEntityManager();
		CarroRepository repository = new CarroRepository(manager);
		repository.adciona(this.carro);
		
		this.carro = new Carro();
	}
	
	
	public Carro getCarro() {
		return carro;
	}


	public void setCarro(Carro carro) {
		this.carro = carro;
	}


	private EntityManager getEntityManager() {
		FacesContext context = FacesContext.getCurrentInstance();
		ExternalContext ec = context.getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ec.getRequest();
		EntityManager manager = (EntityManager) request.getAttribute("EntityManager");
		
		return manager;
	}
}
