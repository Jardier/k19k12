package br.com.wood.k12.controller;

import java.io.Serializable;

import javax.annotation.Resource;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.inject.Model;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import br.com.wood.k12.ejb.ComponenteBean;
import br.com.wood.k12.persistence.entity.Dog;

@Model
@ConversationScoped
public class Controller implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private ComponenteBean componente;

	@Inject
	private EntityManager em;
	
	@Resource
	private UserTransaction tx;

	public void metodo() {
		System.out.println("[Controller] Executando o m�todo");
		componente.medoNegocio();
		System.out.println("[Controller] Entity Manager " + em);
		System.out.println("[Controller] User Transaction " + tx);
		Dog dog = new Dog();
		dog.setNome("Pluto");
		
		try{
			tx.begin();
			em.persist(dog);
			tx.commit();
		}catch(Exception e) {
			
		}
	}
}
