package br.com.wood.k12.converters;

import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import br.com.wood.k12.model.Cpf;

@FacesConverter(forClass = Cpf.class)
public class ConversorCpf implements Converter {

	
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		value = value.trim();

		if (!Pattern.matches("[0-9]{9}-[0-9]{2}", value)) {
			FacesMessage message = new FacesMessage("N�mero de CPF inv�lido");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ConverterException(message);
		}
		String[] partesDoCpf = value.split("-");
		int numeroDeIdentificacao = Integer.parseInt(partesDoCpf[0]);
		int primeiroDigitorVerificador = Integer.parseInt(partesDoCpf[1].substring(0, 1));
		int segundoDigitorVerificador = Integer.parseInt(partesDoCpf[1].substring(1, 2));

		Cpf cpf = new Cpf();
		cpf.setNumeroDeIdentificacao(numeroDeIdentificacao);
		cpf.setPrimeiroDigitorVerificador(primeiroDigitorVerificador);
		cpf.setSegundoDigitorVerificador(segundoDigitorVerificador);

		return cpf;
	}


	public String getAsString(FacesContext context, UIComponent component, Object value) {
		Cpf cpf = (Cpf) value;
		
		if(cpf.getNumeroDeIdentificacao() == null) {
			return "";
		}
		
		return cpf.getNumeroDeIdentificacao() + "-" + cpf.getPrimeiroDigitorVerificador() + cpf.getSegundoDigitorVerificador();
	}

}
