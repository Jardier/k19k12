package br.com.wood.k12.converters;

import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import br.com.wood.k12.model.Telefone;

@FacesConverter(forClass = Telefone.class)
public class TelefoneConverter implements Converter {

	
	public Object getAsObject(FacesContext context, UIComponent component, String value) {

		if (value == null) {
			FacesMessage message = new FacesMessage("N�mero n�o pode ser vazio.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ConverterException(message);
		} 
		value = value.trim();

		if (!Pattern.matches("[0-9]+\\s+[0-9]+\\s+[0-9-]+", value)) {
			FacesMessage message = new FacesMessage("N�mero de telefone inv�lido.");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ConverterException(message);
		} else {
			FacesMessage message = new FacesMessage("Telefone v�lido!");
			message.setSeverity(FacesMessage.SEVERITY_INFO);
			context.addMessage("sucesso", message);

		}

		String campos[] = value.split("\\s+");
		String codigoDoPais = campos[0];
		String codigoDeArea = campos[1];
		String numeroLocal = campos[2];

		Telefone telefone = new Telefone(codigoDoPais, codigoDeArea, numeroLocal);
		return telefone;
	}


	public String getAsString(FacesContext context, UIComponent component, Object value) {
		Telefone telefone = (Telefone) value;
		return telefone.getCodigoPais() + " " + telefone.getCodigoDeArea() + " " + telefone.getNumeroLocal();
	}

}
