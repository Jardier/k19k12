package br.com.wood.k12.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import br.com.wood.k12.model.Curso;

@ManagedBean
public class CursoBean {
	private String siglaCursoEscolhido;
	private List<Curso> cursos = new ArrayList<Curso>();
	private List<String> siglas = new ArrayList<String>();
	private Boolean exAluno;
	
	{
		cursos.add(new Curso("K12","Orientação a Objetos com Java"));
		cursos.add(new Curso("K19","Desenvolvimento Web com JSF2 e JPA2"));
	}

	public String getSiglaCursoEscolhido() {
		return siglaCursoEscolhido;
	}

	public void setSiglaCursoEscolhido(String siglaCursoEscolhido) {
		this.siglaCursoEscolhido = siglaCursoEscolhido;
	}

	public List<Curso> getCursos() {
		return cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}
	 public Boolean getExAluno() {
		return exAluno;
	}
	 
	public void setExAluno(Boolean exAluno) {
		this.exAluno = exAluno;
	}
	 public List<String> getSiglas() {
		return siglas;
	}
	 
	 public void setSiglas(List<String> siglas) {
		this.siglas = siglas;
	}
}
