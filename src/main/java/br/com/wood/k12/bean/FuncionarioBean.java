package br.com.wood.k12.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.wood.k12.model.Funcionario;

@ManagedBean
@ViewScoped
public class FuncionarioBean {
	List<Funcionario> funcionarios = new ArrayList<Funcionario>();
	Funcionario funcionario = new Funcionario();

	public void cadastrar() {
		this.funcionarios.add(this.funcionario);
		this.funcionario = new Funcionario();
	}
	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}
	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}
}
