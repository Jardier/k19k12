package br.com.wood.k12.bean;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class UsuarioBean {
	
	private String nome;
	private Integer idade;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getIdade() {
		return idade;
	}
	public void setIdade(Integer idade) {
		this.idade = idade;
	}
	
	
}
