package br.com.wood.k12.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

@ManagedBean
public class BotaoBean {

	public void sorteiaBotao(ActionEvent event) {
		UIComponent formulario = event.getComponent().getParent();
		
		UIComponent botaoJardier = formulario.findComponent("botao-jardier");
		UIComponent botaoBarbara = formulario.findComponent("botao-barbara");
		UIComponent botaoAdriana = formulario.findComponent("botao-adriana");
		
		botaoJardier.getAttributes().put("disabled", true);
		botaoBarbara.getAttributes().put("disabled", true);
		botaoAdriana.getAttributes().put("disabled", true);
		
		double numero = Math.random();
		
		if(numero < 1.0/3.0){
			botaoJardier.getAttributes().put("disabled", false);
		}else if(numero < 2.0/3.0) {
			botaoBarbara.getAttributes().put("disabled", false);
		}else{
			botaoAdriana.getAttributes().put("disabled", false);
		}
	}
}
