package br.com.wood.k12.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import br.com.wood.k12.model.Instrutor;

@ManagedBean
public class InstrutorBean {
	private List<Instrutor> instrutores = new ArrayList<Instrutor>();

	public InstrutorBean() {
		Instrutor rafael = new Instrutor();
		rafael.setNome("Rafael Cosentino");
		rafael.setDataNascimento("30/10/1984");

		Instrutor marcelo = new Instrutor();
		marcelo.setNome("Marcelo Martins");
		marcelo.setDataNascimento("02/04/1985");

		this.instrutores.add(rafael);
		this.instrutores.add(marcelo);
	}

	public List<Instrutor> getInstrutores() {
		return instrutores;
	}

	public void setInstrutores(List<Instrutor> instrutores) {
		this.instrutores = instrutores;
	}

}
