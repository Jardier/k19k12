package br.com.wood.k12.bean;

import javax.faces.bean.ManagedBean;

import br.com.wood.k12.model.Cpf;

@ManagedBean
public class CPFValidationBean {
	@br.com.wood.k12.beanvalidators.CPF
	private Cpf cpf;

	public Cpf getCpf() {
		return cpf;
	}

	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	
	
}
