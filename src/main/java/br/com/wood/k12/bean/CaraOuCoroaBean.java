package br.com.wood.k12.bean;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class CaraOuCoroaBean {

	public String proximo() {
		if(Math.random() < 0.5){
			return "cara";
		} else {
			return "coroa";
		}
	}
}
