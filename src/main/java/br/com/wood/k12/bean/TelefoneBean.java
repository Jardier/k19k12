package br.com.wood.k12.bean;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;

import br.com.wood.k12.model.Telefone;

@ManagedBean
public class TelefoneBean implements Serializable{
	private static final long serialVersionUID = 2873292431379833489L;
	
	private Telefone telefone;

	public Telefone getTelefone() {
		return telefone;
	}

	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}
	
	

}
