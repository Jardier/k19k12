package br.com.wood.k12.bean;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;

import br.com.wood.k12.model.Cpf;

@ManagedBean
public class CpfBean implements Serializable {
	private static final long serialVersionUID = -3569085835100120337L;
	
	private Cpf cpf = new Cpf();

	public Cpf getCpf() {
		return cpf;
	}

	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	
	
}
