package br.com.wood.k12.bean;

import javax.faces.bean.ManagedBean;

@ManagedBean(name="numeroBean")
public class NumeroAleatorioBean {
	
	private int maximo;
	private int numeroAleatorio;
	
	public String geraNumeroAleatorio() {
		this.numeroAleatorio = (int) (Math.random() * this.maximo);
		return "resposta2";
	}
	
	public int getMaximo() {
		return maximo;
	}
	public int getNumeroAleatorio() {
		return numeroAleatorio;
	}
	
	public void setMaximo(int maximo) {
		this.maximo = maximo;
	}
	
	public void setNumeroAleatorio(int numeroAleatorio) {
		this.numeroAleatorio = numeroAleatorio;
	}

}
