package br.com.wood.k12.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.wood.k12.persistence.entity.Editora;

public class GeraTabela {
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("K19-PU");
		EntityManager manager = factory.createEntityManager();
		
		Editora editora = new Editora();
		editora.setNome("K19 - Livros");
		editora.setEmail("contato@k19.com.br");
		
		manager.getTransaction().begin();
		manager.persist(editora);
		
		manager.getTransaction().commit();
		
		factory.close();
	}

}
