package br.com.wood.k12.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import br.com.wood.k12.model.Cpf;

@FacesValidator("br.com.wood.k12.validator.ValidadorDeCpf")
public class ValidadorDeCpf implements Validator {

	
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		Cpf cpf = (Cpf) value;

		int numeroIdetificacao = cpf.getNumeroDeIdentificacao();
		int primeiroDigitorVerificador = cpf.getPrimeiroDigitorVerificador();
		int segundodigitorVerificador = cpf.getSegundoDigitorVerificador();

		if (!this.validaCPF(numeroIdetificacao, primeiroDigitorVerificador, segundodigitorVerificador)) {
			String numero = numeroIdetificacao + "-" + primeiroDigitorVerificador + segundodigitorVerificador;

			FacesMessage message = new FacesMessage("O n�mero " + numero + " n�o � um CPF v�lido");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
	}

	private boolean validaCPF(int numeroIdetificacao, int primeiroDigitorVerificador, int segundodigitorVerificador) {
		long primeiroDigito = this.modulo11((long) numeroIdetificacao);
		long segundoDigito = this.modulo11((long) numeroIdetificacao * 10 + primeiroDigito);

		return primeiroDigitorVerificador == primeiroDigito && segundodigitorVerificador == segundoDigito;
	}

	private long modulo11(long numero) {
		long soma = 0;
		long multiplicador = 2;
		while (numero > 0) {
			long digito = numero % 10;
			soma += multiplicador * digito;
			numero /= 10;
			multiplicador++;
		}
		long resto = soma % 11;
		if (resto < 2)
			return 0;
		else
			return 11 - resto;
	}

}
