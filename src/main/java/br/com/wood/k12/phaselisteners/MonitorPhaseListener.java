package br.com.wood.k12.phaselisteners;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

public class MonitorPhaseListener implements PhaseListener {
	private static final long serialVersionUID = 1L;

	
	public void afterPhase(PhaseEvent event) {
		System.out.println("MonitorPhaseListener.afterPhase() - " + event.getPhaseId());

	}

	
	public void beforePhase(PhaseEvent event) {
		System.out.println("MonitorPhaseListener.beforePhase() - " + event.getPhaseId());

	}

	
	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}

}
