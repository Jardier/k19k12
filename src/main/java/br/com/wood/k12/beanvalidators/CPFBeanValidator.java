package br.com.wood.k12.beanvalidators;



import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.wood.k12.model.Cpf;

public class CPFBeanValidator implements ConstraintValidator<CPF, br.com.wood.k12.model.Cpf> {
	
	public void initialize(CPF constraintAnnotation) {

	}

	public boolean isValid(Cpf cpf, ConstraintValidatorContext contex) {
		int numeroIdetificacao = cpf.getNumeroDeIdentificacao();
		int primeiroDigitorVerificador = cpf.getPrimeiroDigitorVerificador();
		int segundodigitorVerificador = cpf.getSegundoDigitorVerificador();

		return this.validaCPF(numeroIdetificacao, primeiroDigitorVerificador, segundodigitorVerificador);
	}
	

	private boolean validaCPF(int numeroIdetificacao, int primeiroDigitorVerificador, int segundodigitorVerificador) {
		long primeiroDigito = this.modulo11((long) numeroIdetificacao);
		long segundoDigito = this.modulo11((long) numeroIdetificacao * 10 + primeiroDigito);

		return primeiroDigitorVerificador == primeiroDigito && segundodigitorVerificador == segundoDigito;
	}

	private long modulo11(long numero) {
		long soma = 0;
		long multiplicador = 2;
		while (numero > 0) {
			long digito = numero % 10;
			soma += multiplicador * digito;
			numero /= 10;
			multiplicador++;
		}
		long resto = soma % 11;
		if (resto < 2)
			return 0;
		else
			return 11 - resto;
	}


}
