package br.com.wood.k12.ejb;

import java.io.Serializable;

import javax.ejb.Stateless;

@Stateless
public class ComponenteBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public void medoNegocio() {
		System.out.println("[ComponenteBean] Executando o m�todo de neg�cio....");
	}

}
