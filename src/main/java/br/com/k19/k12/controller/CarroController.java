package br.com.k19.k12.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.inject.Model;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import br.com.k19.k12.model.Carro;

@Model
@ConversationScoped
public class CarroController implements Serializable {
	private static final long serialVersionUID = -8668176447775573849L;

	
	private Carro carro;

	@Inject
	private EntityManager em;

	@Resource
	private UserTransaction tx;

	public CarroController() {
		this.carro = new Carro();
	}

	public void salvar() {
		try {
			tx.begin();
			em.persist(this.carro);
			tx.commit();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.carro = new Carro();
	}
	
	public List<Carro> getCarros() {
		return em.createQuery("SELECT c FROM br.com.k19.k12.model.Carro c", Carro.class).getResultList();
	}

	public Carro getCarro() {
		return carro;
	}

	public void setCarro(Carro carro) {
		this.carro = carro;
	}
}
